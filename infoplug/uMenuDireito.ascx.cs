namespace InfoPlug
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for uMenuDireito.
	/// </summary>
	public class uMenuDireito : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Label lblSemanaExtenso;
		protected System.Web.UI.WebControls.Label lblMesAno;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				string[] Dia = {"Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "S�bado", "Domingo" };

        lblSemanaExtenso.Text = Dia[ (int)DateTime.Now.DayOfWeek ];
				lblMesAno.Text        = String.Format( "{0:00}", DateTime.Now.Month ) + " / " + String.Format( "{0:00}", DateTime.Now.Year );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
