<%@ Page language="c#" Codebehind="quemsomos.aspx.cs" AutoEventWireup="false" Inherits="mesaseetc.quemsomos" %>
<%@ Register TagPrefix="uc1" TagName="Cabecalho" Src="Cabecalho.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>.:: Mesas & Etc ::. Quem Somos</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="principal" cellSpacing="0" cellPadding="0" width="778" align="center" border="0">
				<TR>
					<TD>
						<uc1:Cabecalho id="Cabecalho1" runat="server"></uc1:Cabecalho></TD>
				</TR>
				<TR>
					<TD height="40">
						<TABLE id="Table2" height="40" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD><A href="index.aspx"><IMG onmouseover="MM_swapImage('Image1','','imagens/inicioover.jpg',1)" onmouseout="MM_swapImgRestore()"
											src="imagens/inicioout.jpg" border="0" name="Image1"></A></TD>
								<TD><A href="servicos.aspx"><IMG onmouseover="MM_swapImage('Image2','','imagens/servicosover.jpg',1)" onmouseout="MM_swapImgRestore()"
											src="imagens/servicosout.jpg" border="0" name="Image2"></A></TD>
								<TD><IMG onmouseover="MM_swapImage('Image3','','imagens/fotosover.jpg',1)" onmouseout="MM_swapImgRestore()"
										src="imagens/fotosout.jpg" border="0" name="Image3"></TD>
								<TD><IMG onmouseover="MM_swapImage('Image4','','imagens/prepedidoover.jpg',1)" onmouseout="MM_swapImgRestore()"
										src="imagens/prepedidoout.jpg" border="0" name="Image4"></TD>
								<TD><A href="faleconosco.aspx"><IMG onmouseover="MM_swapImage('Image5','','imagens/faleover.jpg',1)" onmouseout="MM_swapImgRestore()"
											src="imagens/faleout.jpg" border="0" name="Image5"></A></TD>
								<TD><IMG src="imagens/quemselecionado.jpg" border="0" name="Image6"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 19px">
						<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="300" border="0">
							<TR>
								<TD><IMG alt="" src="imagens\lateralesquerda.jpg"></TD>
								<TD><IMG alt="" src="imagens\conteudoquem.jpg"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD><IMG alt="" src="imagens\rodape.jpg"></TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
