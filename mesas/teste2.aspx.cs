using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace mesaseetc
{
	/// <summary>
	/// Summary description for teste2.
	/// </summary>
	public class teste2 : System.Web.UI.Page
	{
    protected System.Web.UI.WebControls.DataList dlFiles;

    public string caminhoAplicacao;
    public string Section;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{

        caminhoAplicacao = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
        System.IO.DirectoryInfo diretorio = new System.IO.DirectoryInfo(caminhoAplicacao + "imagens/thumb/");
        System.IO.FileInfo[] arquivos =  diretorio.GetFiles("*.jpg");
        dlFiles.DataSource = arquivos;
        dlFiles.DataBind();

/*
  					<a href="#1" onclick="window.open('popup.aspx?img=imagens\galeria\tamanhonormal\<%# ((System.IO.FileInfo)Container.DataItem).Name %>', 'nwwin', 'toolbar=0,location=0,directories=0,status=0,menubar=no,scrollbars=yes,resizable=no,width=605,height=495');">
						<img src='<%=System.Configuration.ConfigurationSettings.AppSettings["ImagesFolder"]%><%=Section%>/<%# ((System.IO.FileInfo)Container.DataItem).Name %>' border="0" width=55 height=55 id="Simg"></a>

 * 
 */
      }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
      this.dlFiles.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.dlFiles_ItemDataBound);
      this.Load += new System.EventHandler(this.Page_Load);

    }
		#endregion


    private void dlFiles_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
    {


      ((System.Web.UI.WebControls.Image)e.Item.FindControl("thumb")).Attributes.Add(
"onClick", "javascript:window.open('popup.aspx?img=" +  
        ((System.IO.FileInfo)e.Item.DataItem).Name + "'" + ", 'nwwin', 'toolbar=0,location=0,directories=0,status=0,menubar=no,scrollbars=no,resizable=no,width=316,height=400'); ");

// o codigo abaixo apresentou problemas no firefox, simplesmente pq eu estava pegando o caminho local 
// e ISTO ESTA ERRADO!!
//      ((System.Web.UI.WebControls.Image)e.Item.FindControl("thumb")).ImageUrl = 
//        System.Configuration.ConfigurationSettings.AppSettings["ImagesFolder"] + @"/" + ((System.IO.FileInfo)e.Item.DataItem).Name;

      ((System.Web.UI.WebControls.Image)e.Item.FindControl("thumb")).ImageUrl = 
        System.Configuration.ConfigurationSettings.AppSettings["imagensThumb"] + ((System.IO.FileInfo)e.Item.DataItem).Name;
    
    }
	}
}
