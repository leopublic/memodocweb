<%@ Page language="c#" Codebehind="faleconosco.aspx.cs" AutoEventWireup="false" Inherits="mesaseetc.faleconosco" %>
<%@ Register TagPrefix="uc1" TagName="Cabecalho" Src="Cabecalho.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>.:: Mesas & Etc ::. Fale Conosco</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="principal" cellSpacing="0" cellPadding="0" width="778" align="center" border="0">
				<TR>
					<TD>
						<uc1:Cabecalho id="Cabecalho1" runat="server"></uc1:Cabecalho></TD>
				</TR>
				<TR>
					<TD height="40">
						<TABLE id="Table2" height="40" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<td><a href="index.aspx"><img src="imagens/inicioout.jpg" name="Image1" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','imagens/inicioover.jpg',1)"
											border="0"></a></td>
								<TD><a href="servicos.aspx"><img src="imagens/servicosout.jpg" name="Image2" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image2','','imagens/servicosover.jpg',1)"
											border="0"></a></TD>
								<TD><img src="imagens/fotosout.jpg" name="Image3" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image3','','imagens/fotosover.jpg',1)"
										border="0"></TD>
								<TD><img src="imagens/prepedidoout.jpg" name="Image4" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image4','','imagens/prepedidoover.jpg',1)"
										border="0"></TD>
								<TD><img src="imagens/faleselecionado.jpg" name="Image5"></TD>
								<TD><a href="quemsomos.aspx"><img src="imagens/quemout.jpg" name="Image6" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6','','imagens/quemover.jpg',1)"
											border="0"></a></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="300" border="0">
							<TR>
								<TD>
									<TABLE id="lateral" height="285" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD><IMG alt="" src="imagens\lateralesquerda.jpg" width="249"></TD>
										</TR>
									</TABLE>
								</TD>
								<TD>
									<TABLE id="Table1" height="286" cellSpacing="0" cellPadding="0" width="519" border="0">
										<TR>
											<TD background="imagens\backfale.jpg" vAlign="top">
												<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="300" border="0">
													<TR>
														<TD>
															<P>&nbsp;</P>
														</TD>
													</TR>
													<TR>
														<TD>
															<P><IMG alt="" src="imagens\titulofaleconosco.jpg"></P>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="300" align="center" border="0">
																<TR>
																	<TD style="WIDTH: 110px; HEIGHT: 28px" align="right">
																		<asp:Label id="Label1" runat="server" Font-Names="Verdana" Font-Size="8pt" Font-Bold="True">Nome:</asp:Label></TD>
																	<TD style="WIDTH: 286px; HEIGHT: 28px">&nbsp;
																		<asp:TextBox id="txtNome" runat="server" BorderStyle="Groove" Width="262px" ForeColor="#0000C0"></asp:TextBox></TD>
																	<TD style="WIDTH: 82px; HEIGHT: 28px"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 110px; HEIGHT: 26px" align="right">
																		<asp:Label id="Label2" runat="server" Font-Names="Verdana" Font-Size="8pt" Font-Bold="True">E-mail:</asp:Label></TD>
																	<TD style="WIDTH: 286px; HEIGHT: 26px">&nbsp;
																		<asp:TextBox id="txtEmail" runat="server" BorderStyle="Groove" Width="268px"></asp:TextBox></TD>
																	<TD style="WIDTH: 82px; HEIGHT: 26px"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 110px; HEIGHT: 27px" align="right">
																		<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="8pt" Font-Bold="True">Assunto:</asp:Label></TD>
																	<TD style="WIDTH: 286px; HEIGHT: 27px">&nbsp;
																		<asp:TextBox id="txtAssunto" runat="server" BorderStyle="Groove" Width="268px"></asp:TextBox></TD>
																	<TD style="WIDTH: 82px; HEIGHT: 27px"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 110px" align="right">
																		<asp:Label id="Label4" runat="server" Font-Names="Verdana" Font-Size="8pt" Font-Bold="True">Texto:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 286px">&nbsp;
																		<asp:TextBox id="txtTexto" runat="server" BorderStyle="Groove" Width="269px" TextMode="MultiLine"
																			Height="108px"></asp:TextBox></TD>
																	<TD style="FONT-SIZE: 5pt; COLOR: white; FONT-FAMILY: 'Arial Narrow'" vAlign="bottom"
																		noWrap>aa
																		<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="imagens\btnEnviar.jpg"></asp:ImageButton></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD><IMG alt="" src="imagens\rodape.jpg"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
