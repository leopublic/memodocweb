using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;


namespace mesaseetc
{
	/// <summary>
	/// Summary description for popup.
	/// </summary>
	public class popup : System.Web.UI.Page
	{
    protected System.Web.UI.WebControls.Image Image1;
    protected System.Web.UI.WebControls.Label lblDescricao;
    protected System.Web.UI.WebControls.Label lblArquivo;
    public string diretorio;
		
    private void Page_Load(object sender, System.EventArgs e)
    {
      diretorio = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
      string nomeImagem = Request["img"]; // nao utilizado mais (utizado com o componente HTML img

      string nomeImagem1 = System.Configuration.ConfigurationSettings.AppSettings["tamanhoNormal"] + Request["img"];

      nomeImagem = nomeImagem.Substring(nomeImagem.LastIndexOf("/") + 1, nomeImagem.Length - nomeImagem.LastIndexOf("/") -1);
      Image1.ImageUrl = nomeImagem1;

      //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path + "English/images/gallery/Thumb/");
      //string sDescFileName = System.Configuration.ConfigurationSettings.AppSettings["ImageDescFile"] ;


      string sDescFileName = (diretorio + "xml/popup_descricao.xml");

      XmlDocument doc = new XmlDocument();
      doc.Load(sDescFileName);

      XmlNode node = doc.SelectSingleNode("/imagens/img[@nome='"+ nomeImagem +"']/desc");
      if(node != null)
      {
        lblDescricao.Text = 	node.InnerText; 
        lblArquivo.Text   =   nomeImagem;
      }


      // Put user code to initialize the page here
    }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
      this.Load += new System.EventHandler(this.Page_Load);

    }
		#endregion
	}
}
